// const svArr = [
//     {id: "hihi", name: "bảo"}
// ]
// function* themSV (a) { // trả về nhìu giá trị sau nhiều lần gọi
//     yield [...svArr, a] // giông như lệh return 
//     yield "thêm sinh viên  thành công"
//     yield "hihi"

//     return "thành công"
// }

// const a = themSV({id: "haha", name: "sang"});
// console.log(a.next().value);
// console.log(a.next().value);
// console.log(a.next().value);
// console.log(a.next());


//redux saga  fork take 

function* hihi(count) {
    var count = 1;
    while (true) {
        count += 1;
        yield count
    }
}

let f = hihi();
console.log(f.next());